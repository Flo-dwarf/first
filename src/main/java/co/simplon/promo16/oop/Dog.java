package co.simplon.promo16.oop;
/**
 * Une première classe qui représente un chien.
 */
public class Dog {
    public String name;
    public String breed;
    public int age;


    public void greeting() {
        System.out.println("Hello, my name is "+name+
        ", I'm a "+breed+" and I'm "+age+" years old. Woof Woof");
    }

    public void bark(String something) {
        System.out.println("Dogs bark on "+something);
    }

    
}
