# Premier projet java
Un premier projet java généré avec l'archetype maven quickstart

## Exo conditions + variable  (App.java => méthode conditionExplanation)
1. Créer une variable avec un age dedans, mettez la valeur que vous voulez dedans
2. Créer une condition qui affichera quelque chose si l'age est supérieur à 18 
3. Créer une autre variable ageLimit et faire en sorte de modifier la condition précédente pour comparer l'age et l'ageLimit
4. Créer une variable contenant une chaîne de caractère et faire une condition qui dit que si c'est votre prénom dans la variable, ça affiche "bonjour " suivit du contenu de la variable, sinon faire que ça affiche "casse toi"
5. Créer une autre condition qui va vérifier le nombre de caractère dans la variable prenom, et s'il y a autant de caractère que la valeur de age, alors ça met un message
6. Faire une variable qui va contenir un int puis faire une condition qui affichera even ou odd selon si le nombre est pair ou impair